docker volume create portainer_data
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

python ./web/create_database_dockerfile.py

docker-compose build # build docker containers
docker-compose up -d # start docker containers

docker-compose run --rm web python ./create_database.py # Create web database

docker-compose run --rm scraper python ./Scraper.py gomovies site_type=series episode=True site_type=series # Run Scraper