import sys, os
from project import app

# Database Initialization Files
docker_file = 'Dockerfile'
source_dir = os.path.abspath(os.curdir)
destination_dir = os.path.join(source_dir, 'mysql')

# Before creating files, check that the destination directory exists
if not os.path.isdir(destination_dir):
    os.makedirs(destination_dir)

# Create the 'Dockerfile' for initializing the Database Docker image
with open(os.path.join(destination_dir, docker_file), 'w') as mysql_dockerfile:
    mysql_dockerfile.write('FROM mysql')
    mysql_dockerfile.write('\n')
    mysql_dockerfile.write('\n# Set environment variables')
    mysql_dockerfile.write('\nENV MYSQL_USER {}'.format(app.config['MYSQL_USER']))
    mysql_dockerfile.write('\nENV MYSQL_PASSWORD {}'.format(app.config['MYSQL_PASSWORD']))
    mysql_dockerfile.write('\nENV MYSQL_ROOT_PASSWORD {}'.format(app.config['MYSQL_ROOT_PASSWORD']))
    mysql_dockerfile.write('\nENV MYSQL_DATABASE {}'.format(app.config['MYSQL_DATABASE']))
    mysql_dockerfile.write('\n')

# Notes:
# - Docker commands - https://wiki.archlinux.org/index.php/Docker
# - Reads python config, generates docker database credentials - web/create_database_dockerfile.py