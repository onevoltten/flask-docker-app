# manage.py

import os
import unittest
import coverage
import datetime
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask_admin import Admin
from werkzeug.contrib.profiler import ProfilerMiddleware

COV = coverage.coverage(branch=True, include='project/*', omit=['*/__init__.py', '*/config/*'])
COV.start()

from project import app, db, MyAdminIndexView
from project.model.user import Users, UsersView
from project.model.title import Title, TitleView
from project.model.episode import Episode, EpisodeView

app.jinja_env.cache = {}
app.config.from_pyfile('config.py')

migrate = Migrate(app, db)

admin = Admin(app, name='admin_name', index_view=MyAdminIndexView(), base_template='my_master.html')
admin.add_view(UsersView(Users, db.session))
admin.add_view(TitleView(Title, db.session))
admin.add_view(EpisodeView(Episode, db.session))

manager = Manager(app)

manager.add_command('db', MigrateCommand) # migrations

@manager.command # no coverage
def test():
    tests = unittest.TestLoader().discover('project/tests')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful(): return 0
    else: return 1

@manager.command
def proflier():
    app.config['PROFILE'] = True
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions = [30])
    app.run(debug = True)

@manager.command # with coverage
def cov():
    tests = unittest.TestLoader().discover('project/tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
    COV.stop()
    COV.save()
    print('Coverage Summary:')
    COV.report()
    basedir = os.path.abspath(os.path.dirname(__file__))
    covdir = os.path.join(basedir, 'tmp/coverage')
    COV.html_report(directory=covdir)
    print('HTML version: file://%s/index.html' % covdir)
    COV.erase()

@manager.command
def create_db(): db.create_all()

@manager.command
def drop_db(): db.drop_all()

@manager.command
def create_admin():
    db.session.add(Users( email="ad@min.com", password="admin", admin=True, confirmed=True, confirmed_on=datetime.datetime.now()))
    db.session.commit()

if __name__ == '__main__':
    manager.run()