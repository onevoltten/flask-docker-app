import datetime
# Create the database tables, add some initial data, and commit to the database
from project import db
from project.model.user import Users
from project.model.title import Title
from project.model.episode import Episode

# Drop all of the existing database tables
# db.drop_all()

# Create the database and the database table
db.create_all()

# Insert user data
db.session.add(Users( email="ad@min.com", password="admin", admin=True, confirmed=True, confirmed_on=datetime.datetime.now()))
db.session.commit()

'''
# Insert recipe data
recipe1 = Recipe('Slow-Cooker Tacos', 'Delicious ground beef that has been simmering in taco seasoning and sauce.  Perfect with hard-shelled tortillas!', user2.id, False)
recipe2 = Recipe('Hamburgers', 'Classic dish elevated with pretzel buns.', user2.id, True)
recipe3 = Recipe('Mediterranean Chicken', 'Grilled chicken served with pitas, hummus, and sauted vegetables.', user2.id, True)
db.session.add(recipe1)
db.session.add(recipe2)
db.session.add(recipe3)

# Commit the changes for the recipes
db.session.commit()
'''
print('Migrated!')
