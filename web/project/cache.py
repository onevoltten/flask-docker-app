from flask_caching import Cache

from project import app

cache = Cache(app, config={'CACHE_TYPE': app.config['CACHE_TYPE']})