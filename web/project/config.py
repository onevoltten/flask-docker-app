# project/config.py

import os
import configparser

basedir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig(object):
    # main config
    DEBUG = False
    DEBUG_TB_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False

    SECRET_KEY = 'super_secret'
    SECURITY_PASSWORD_SALT = 'super_secret_salty'

    STRIPE_SECRET_KEY = None
    STRIPE_PUBLISHABLE_KEY = None

    MYSQL_USER = 'onevoltten'
    MYSQL_PASSWORD = 'MYSQL_PASSWORD'
    MYSQL_ROOT_PASSWORD = 'MYSQL_PASSWORD'
    MYSQL_DATABASE = 'test'

    SQLALCHEMY_DATABASE_URI = 'mysql://' + MYSQL_USER + ':' + MYSQL_PASSWORD + '@localhost/' + MYSQL_DATABASE

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True
    SQLALCHEMY_BINDS = {}
    DATABASE_QUERY_TIMEOUT = 0.5

    BCRYPT_LOG_ROUNDS = 13

    WTF_CSRF_ENABLED = True
    
    CACHE_TYPE = 'null'

    CDN_DOMAIN = ''

    # mail config
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = int(465)
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''
    MAIL_DEFAULT_SENDER = 'noreply@sender.com'

class DevelopmentConfig(BaseConfig):
    DEBUG = True
    WTF_CSRF_ENABLED = False
    CACHE_TYPE = 'simple'

class TestingConfig(BaseConfig):
    #LOGIN_DISABLED = False # TODO test removed
    TESTING = True
    BCRYPT_LOG_ROUNDS = 1
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_RECORD_QUERIES = False

class ProductionConfig(BaseConfig):
    CACHE_TYPE = 'simple'

	# mail settings
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''

    # Cache
    CACHE_TYPE = 'simple'

    # CDN Domain
    CDN_DOMAIN = ''

    MYSQL_USER = 'onevoltten'
    MYSQL_PASSWORD = 'MYSQL_PASSWORD'
    MYSQL_ROOT_PASSWORD = 'MYSQL_PASSWORD'
    MYSQL_DATABASE = 'test'

    SQLALCHEMY_DATABASE_URI = 'mysql://' + MYSQL_USER + ':' + MYSQL_PASSWORD + '@mysql:3306/' + MYSQL_DATABASE