var options = {
    url: function(phrase) {
      return "api/search/"+phrase+"/";
    },
    getValue: function(element) {
      return element.name;
    },
    template: {
      type: "links",
      fields: { link: "id" }
    },
    requestDelay: 200,
    placeholder: "Search",
    list: {
        maxNumberOfElements: 5,
        match: { enabled: true},
        sort: { enabled: true },
        showAnimation: {
          type: "fade",
          time: 300,
          callback: function() {}
        },
        hideAnimation: {
          type: "fade",
          time: 300,
          callback: function() {}
        }
    }
};
$("#inputOne").easyAutocomplete(options);
