function ajaxContentBox(a) {
    $("div#" + a + " #content-box").is(":empty") && $.ajax({
        url: "https://website.com/ajax/get_content_box/" + a,
        type: "GET",
        dataType: "json",
        success: function (a) {
            switch (a.type) {
                case "topview-today":
                    $("#topview-today #content-box").html(a.content);
                    break;
                case "top-favorite":
                    $("#top-favorite #content-box").html(a.content);
                    break;
                case "top-rating":
                    $("#top-rating #content-box").html(a.content);
                    break;
                case "top-imdb":
                    $("#top-imdb #content-box").html(a.content)
            }
        }
    })
}

function searchMovie() {
    var a = $("input[name=keyword]").val();
    "" !== a.trim() && (a = a.replace(/(<([^>]+)>)/gi, "").replace(/[`~!@#$%^&*()_|\=?;:'",.<>\{\}\[\]\\\/]/gi, ""), a = a.split(" ").join("+"), window.location.href = "https://website.com/search/" + a)
}