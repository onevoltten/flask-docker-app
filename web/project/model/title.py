#!/usr/bin/env python3

from project import db
from flask import render_template, flash
from flask_login import current_user
from flask_admin.contrib.sqla import ModelView
import math

tags = db.Table('data_map',
    db.Column('tag_id', db.Integer, db.ForeignKey('manytable.id'), primary_key=True),
    db.Column('post_id', db.Integer, db.ForeignKey('title.id'), primary_key=True)
)

class ManyTable_Name(db.Model):
    __tablename__ = "manytable_name"

    id = db.Column(db.Integer, primary_key=True)
    table_id = db.Column(db.Integer)
    table_data = db.Column(db.String(10000))

    def __str__(self): return self.table_data

class ManyTable(db.Model):
    __tablename__ = "manytable"

    id = db.Column(db.Integer, primary_key=True)
    table_id = db.Column(db.Integer, db.ForeignKey('manytable_name.id'))
    table_data = db.Column(db.String(9999))

    def __str__(self): return self.table_data

class Title(db.Model):
    __tablename__ = "title"

    id = db.Column(db.Integer, primary_key=True)
    url_slug = db.Column(db.String(100), unique=True)
    season = db.Column(db.String(100))
    title = db.Column(db.String(200))
    title_old = db.Column(db.String(200))
    fallback = db.Boolean(db.Integer)
    episodes = db.Column(db.Integer)
    duration = db.Column(db.Integer)
    summary = db.Column(db.String(2000))
    trailer_youtube = db.Column(db.String(20))
    img_poster = db.Column(db.String(500))
    img_poster_alt = db.Column(db.String(500))
    img_background = db.Column(db.String(500))
    imdb_rating = db.Column(db.String(10))
    imdb_votes = db.Column(db.String(10))
    imdb_id = db.Column(db.String(20))
    thetvdb_id = db.Column(db.String(20))
    tvmaze_id = db.Column(db.String(20))
    released = db.Column(db.Date)
    last_update = db.Column(db.Date)
    views = db.Column(db.Integer, default=0, nullable=False)
    published = db.Column(db.Boolean, nullable=False, default=False)
    children = db.relationship('ManyTable', secondary=tags, backref=db.backref('parents', lazy='dynamic'))
    
    def __str__(self): return str(self.children)
    def __str__(self): return self.name


'''
class Data_Map(db.Model):
    __tablename__ = "data_map"

    post_id = db.Column(db.Integer, db.ForeignKey('title.id'), primary_key=True)
    tag_id = db.Column(db.Integer, db.ForeignKey('manytable.id'), primary_key=True)

    def __str__(self): return self.tag_id
'''
class Title_GM(db.Model):
    __tablename__ = "title_gm"

    id = db.Column(db.Integer, primary_key=True)
    id_title = db.Column(db.Integer, db.ForeignKey('title.id'))
    id_gm = db.Column(db.Integer)
    season = db.Column(db.Integer)

    def __str__(self): return self.season

# ------ other ------ #

def film_detail(slug):
    return Title.query.filter(Title.children.any(Title.url_slug == slug)).all()

def pagination(page, page_type, parameter, per_page=32):
    if page_type == "page":
        datax = Title.query.order_by(Title.released.desc()).paginate(page,per_page,error_out=False)
        data = datax.items
        if len(datax.items) < per_page: pages = 1 # TODO test
        else: pages = len(datax.items)
    else:
        flash('Unknown page type', 'warning')
        return render_template('errors/404.html'), 404
    return data, pages

class TitleView(ModelView):
    def is_accessible(self):
        if current_user.is_authenticated:
            if current_user.admin: return True
            else: return False
        else: return False