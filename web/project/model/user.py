#!/usr/bin/env python3

import datetime
from project import db, bcrypt
from flask_login import current_user
from flask_admin.contrib.sqla import ModelView
class Users(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)
    registered_on = db.Column(db.DateTime)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    confirmed_on = db.Column(db.DateTime)
    password_reset_token = db.Column(db.String(100), nullable=True)

    def __init__(self, email, password, confirmed,
                 admin=False, confirmed_on=None,
                 password_reset_token=None):
        self.email = email
        self.password = bcrypt.generate_password_hash(password)
        self.registered_on = datetime.datetime.now()
        self.admin = admin
        self.confirmed = confirmed
        self.confirmed_on = confirmed_on
        self.password_reset_token = password_reset_token

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def __repr__(self):
        return '<email {}'.format(self.email)

# Create customized model view class
class UsersView(ModelView):

    def is_accessible(self):
        if current_user.is_authenticated:
            if current_user.admin:
                return True
            else:
                return False
        else:
            return False


