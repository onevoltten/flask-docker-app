#!/usr/bin/env python3

from project import db
from flask_login import current_user
from flask_admin.contrib.sqla import ModelView


class Episode(db.Model):
    __tablename__ = "episode"

    id = db.Column(db.Integer, primary_key=True)
    id_title = db.Column(db.Integer, db.ForeignKey('title.id'), nullable=False)
    season = db.Column(db.Integer)
    episode = db.Column(db.Integer)
    episode_end = db.Column(db.Integer)
    title = db.Column(db.String(200))
    summary = db.Column(db.String(2000))
    img_medium = db.Column(db.String(500))
    img_original = db.Column(db.String(500))
    duration = db.Column(db.String(50))
    data = db.Column(db.String(500))
    filesize = db.Column(db.String(50))
    airdate = db.Column(db.Date)
    imdb_rating = db.Column(db.String(10))
    imdb_votes = db.Column(db.String(10))
    imdb_id = db.Column(db.String(20))
    tvmaze_id = db.Column(db.String(20))
    host_id = db.Column(db.Integer)
    filetype_id = db.Column(db.Integer)
    resolution_id = db.Column(db.Integer)

class EpisodeView(ModelView):
    def is_accessible(self):
        if current_user.is_authenticated:
            if current_user.admin: return True
            else: return False
        else: return False