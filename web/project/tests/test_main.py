import unittest
from project.util import BaseTestCase

class TestMainViews(BaseTestCase):
    def test_login_route(self):
        response = self.client.get('/login', follow_redirects=True)
        self.assertTrue(response.status_code == 200)
        self.assertTemplateUsed('user/login.html')

if __name__ == '__main__':
    unittest.main()