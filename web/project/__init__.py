# project/__init__.py

#################
#### imports ####
#################
import os, warnings
from flask import Flask, render_template, request, redirect, flash, url_for
from flask_login import logout_user, LoginManager, login_user, current_user
from flask_bcrypt import Bcrypt
from flask_mail import Mail
from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy, get_debug_queries
from flask_admin import AdminIndexView, expose, helpers
from flask_admin.contrib.sqla import ModelView
from flask_cdn import CDN
from flask_compress import Compress

################
#### config ####
################

app = Flask(__name__)

try:
    APP_SET = os.environ['APP_SETTINGS']
    app.config.from_object("project.config."+APP_SET+"Config")
except KeyError:
    app.config.from_object("project.config.ProductionConfig") # Assume

####################
#### extensions ####
####################

login_manager = LoginManager()
login_manager.init_app(app)
bcrypt = Bcrypt(app)
mail = Mail(app)
toolbar = DebugToolbarExtension(app)
db = SQLAlchemy(app)
cdn = CDN(app)
Compress(app)

###############
#### cache ####
###############

from project.cache import cache

####################
#### blueprints ####
####################

from project.main.views import main_blueprint
from project.user.views import user_blueprint
app.register_blueprint(main_blueprint)
app.register_blueprint(user_blueprint)

#####################
#### flask-login ####
#####################
from wtforms import form, fields, validators
from project.model.user import Users
from werkzeug.security import check_password_hash

# Define login and registration forms (for flask-login)
class LoginForm(form.Form):
    email = fields.StringField(validators=[validators.required()])
    password = fields.PasswordField(validators=[validators.required()])

    def validate_email(self, field):
        user = self.get_user()
        if user is None: raise validators.ValidationError('Invalid email')
        if not bcrypt.check_password_hash(user.password, self.password.data): raise validators.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(Users).filter_by(email=self.email.data).first()

# Create customized model view class
class MyModelView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated

# Create customized index view class that handles login & registration
class MyAdminIndexView(AdminIndexView):
    @expose('/')
    def index(self):
        if not current_user.is_authenticated: return redirect(url_for('.login_view'))
        return super(MyAdminIndexView, self).index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        # user login
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login_user(user)

        if current_user.is_authenticated: return redirect(url_for('.index'))
        self._template_args['form'] = form
        return super(MyAdminIndexView, self).index()

    @expose('/logout/')
    def logout_view(self):
        logout_user()
        return redirect(url_for('.index'))


# Initialize flask-login
def init_login():
    login_manager = LoginManager()
    login_manager.init_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id): return db.session.query(Users).get(user_id)

    @login_manager.unauthorized_handler
    def unauthorized_callback(): return redirect('/login?next=' + request.path)

# Initialize flask-login
init_login()

########################
#### error handlers ####
########################

@app.errorhandler(403)
@cache.cached(timeout=3600)
def forbidden_page(error):
    app.logger.info('Forbidden page - [{}] [{}]'.format(request.path, error))
    return render_template("errors/403.html"), 403

@app.errorhandler(404)
@cache.cached(timeout=3600)
def page_not_found(error):
    return render_template("errors/404.html"), 404

@app.errorhandler(500)
@cache.cached(timeout=3600)
def server_error_page(error):
    app.logger.warning('Server error - [] [{}]'.format(request.path, error))
    return render_template("errors/500.html"), 500

@app.after_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= app.config['DATABASE_QUERY_TIMEOUT']:
            app.logger.warning("Database slow: {}\nParameters: {}\nDuration: {}s\nContext: {}".format(query.statement, query.parameters, query.duration, query.context))
    return response