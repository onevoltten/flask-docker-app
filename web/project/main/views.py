# project/main/views.py

#################
#### imports ####
#################

import re
from flask import Blueprint, redirect, url_for, render_template, flash
from flask_login import current_user
from flask_login import login_required
from project.email import send_email
from project.model.title import Title, film_detail, pagination
from project.cache import cache

################
#### config ####
################

main_blueprint = Blueprint('main', __name__,)

################
#### routes ####
################

@main_blueprint.route('/')
@main_blueprint.route('/filter/<path:page_type>/<path:parameter>/<int:page_index>',methods=['GET'])
@cache.cached(timeout=30)
def home(page_type='page',page_index=1,parameter='n'):
    posts, page_total = pagination(page_index, page_type, parameter)
    return render_template('pagination/index.html',page_index=page_index, page_total=page_total, posts=posts, page_type=page_type, parameter=parameter)

@main_blueprint.route("/film/<string:slug>/")
@main_blueprint.route("/watchseries/<string:slug>/")
@cache.cached(timeout=30)
def title(slug):
    try: film_data = film_detail(slug)[0]
    except IndexError: return render_template('errors/404.html'), 404
    return render_template('/main/title.html',film_id=slug,film_data=film_data)